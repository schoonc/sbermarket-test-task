import React from 'react';
import SmSliderForm from "./components/SmSliderForm";

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.handleStartChange = this.handleStartChange.bind(this)
    this.handleEndChange = this.handleEndChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.state = {
      min: 0,
      max: 100,
      start: 0,
      end: 100
    };
  }
  handleStartChange(value) {
    this.setState({
      start: value
    })
  }
  handleEndChange(value) {
    this.setState({
      end: value
    })
  }
  handleSubmit(e) {
    e.preventDefault()
    const data = {
      start: this.state.start,
      end: this.state.end
    }
    console.log('submit data: ', data)
  }
  render() {
    return (<SmSliderForm
      min={this.state.min}
      max={this.state.max}
      start={this.state.start}
      end={this.state.end}
      onStartChange={this.handleStartChange}
      onEndChange={this.handleEndChange}
      onSubmit={this.handleSubmit}
    />);
  }
}
