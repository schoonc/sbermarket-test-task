import React from 'react';
import './SmSlider.css';
import SmButton from "./SmButton";

const THUMB_TYPE_START = 'start'
const THUMB_TYPE_END = 'end'

export default class SmSlider extends React.Component {
  constructor(props) {
    super(props);
    this.handleStartChange = this.handleStartChange.bind(this)
    this.handleEndChange = this.handleEndChange.bind(this)
    this.handleMouseUp = this.handleMouseUp.bind(this)
    this.handleMouseMove = this.handleMouseMove.bind(this)
    this.handleThumbsMouseDown = this.handleThumbsMouseDown.bind(this)
    this.handleResetValues = this.handleResetValues.bind(this)
    this.thumbsRef = React.createRef()
    this.activeThumbType = null
  }

  componentWillUnmount() {
    document.removeEventListener('mousemove', this.handleMouseMove)
    document.removeEventListener('mouseup', this.handleMouseUp)
  }

  valueWithinRange(value) {
    return (value >= this.props.min && value <= this.props.max)
  }

  handleStartChange(e) {
    const value = parseFloat(e.target.value);
    if (!this.valueWithinRange(value)) return
    this.props.onStartChange(value)
  }

  handleEndChange(e) {
    const value = parseFloat(e.target.value);
    if (!this.valueWithinRange(value)) return
    this.props.onEndChange(value)
  }

  getValues(e) {
    const {left: trackStart, width: trackLength} = this.thumbsRef.current.getBoundingClientRect()
    const mousePos = e.clientX
    const part = Math.min(Math.max((mousePos - trackStart) / trackLength, 0), 1)
    const value = this.props.min + part * (this.props.max - this.props.min)
    const roundedValue = Math.round(value)

    return {
      value,
      roundedValue
    }
  }

  handleMouseMove(e) {
    const {roundedValue} = this.getValues(e)

    if (this.activeThumbType === THUMB_TYPE_START) {
      if (roundedValue <= this.props.end) {
        this.props.onStartChange(roundedValue);
      } else {
        this.activeThumbType = THUMB_TYPE_END
        this.props.onStartChange(this.props.end)
        this.props.onEndChange(roundedValue);
      }
    } else {
      if (roundedValue >= this.props.start) {
        this.props.onEndChange(roundedValue)
      } else {
        this.activeThumbType = THUMB_TYPE_START
        this.props.onEndChange(this.props.start)
        this.props.onStartChange(roundedValue)
      }
    }
  }

  getNearestThumb(value) {
    return Math.abs(value - this.props.start) < Math.abs(value - this.props.end)
      ? THUMB_TYPE_START
      : THUMB_TYPE_END
  }

  handleThumbsMouseDown(e) {
    if (this.activeThumbType) return

    const {value, roundedValue} = this.getValues(e)

    const nearestThumb = this.getNearestThumb(value)

    if (nearestThumb === THUMB_TYPE_START) {
      this.props.onStartChange(roundedValue)
      this.startTracking(THUMB_TYPE_START)
    } else {
      this.props.onEndChange(roundedValue)
      this.startTracking(THUMB_TYPE_END)
    }
  }

  handleThumbMouseDown(thumbType) {
    this.startTracking(thumbType)
  }

  handleMouseUp() {
    this.activeThumbType = null
    document.removeEventListener('mousemove', this.handleMouseMove)
    document.removeEventListener('mouseup', this.handleMouseUp)
  }

  startTracking(thumbType) {
    this.activeThumbType = thumbType

    document.addEventListener('mousemove', this.handleMouseMove)
    document.addEventListener('mouseup', this.handleMouseUp)
  }

  handleResetValues(e) {
    this.props.onStartChange(this.props.min)
    this.props.onEndChange(this.props.max)
  }

  render() {
    const startPosition = (this.props.start - this.props.min) / ((this.props.max - this.props.min) / 100);
    const endPosition = (this.props.end - this.props.min) / ((this.props.max - this.props.min) / 100);
    const leftBackgroundStyle = {width: `${startPosition}%`}
    const fillStyle = {left: `${startPosition}%`, width: `${endPosition - startPosition}%`}
    const rightBackgroundStyle = {width: `${100 - endPosition}%`}

    return (
      <div className="sm-slider">
        <div className="sm-slider__inputs">
          <div className="sm-slider__input-wrapper">
            <input
              className="sm-slider__input"
              type="number"
              min={this.props.min}
              max={this.props.max}
              value={this.props.start}
              onChange={this.handleStartChange}
            />
          </div>
          <div className="sm-slider__input-wrapper">
            <input
              className="sm-slider__input"
              type="number"
              min={this.props.min}
              max={this.props.max}
              value={this.props.end}
              onChange={this.handleEndChange}
            />
          </div>
        </div>
        <div className="sm-slider__thumbs" ref={this.thumbsRef} onMouseDown={this.handleThumbsMouseDown}>
          <div className="sm-slider__track-background" style={leftBackgroundStyle}/>
          <div className="sm-slider__track-fill" style={fillStyle}/>
          <div className="sm-slider__track-background" style={rightBackgroundStyle}/>
          <div
            className="sm-slider__thumb"
            style={{left: `${startPosition}%`}}
            onMouseDown={(e) => this.handleThumbMouseDown(THUMB_TYPE_START, e)}
          />
          <div
            className="sm-slider__thumb"
            style={{left: `${endPosition}%`}}
            onMouseDown={(e) => this.handleThumbMouseDown(THUMB_TYPE_END, e)}
          />
        </div>
        <div className="sm-slider__reset">
          <SmButton onClick={this.handleResetValues}>Сбросить значения</SmButton>
        </div>
      </div>
    );
  }
}