import React from 'react';
import SmSlider from "./SmSlider";
import SmButton from './SmButton';
import './SmSliderForm.css';

export default class SmSliderForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleStartChange = this.handleStartChange.bind(this)
    this.handleEndChange = this.handleEndChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  handleStartChange(value) {
    this.props.onStartChange(value)
  }

  handleEndChange(value) {
    this.props.onEndChange(value)
  }

  onSubmit(e) {
    this.props.onSubmit(e)
  }

  render() {
    return (
      <form style={{width: '500px', margin: '0 auto'}} onSubmit={this.onSubmit}>
        <SmSlider
          min={this.props.min}
          max={this.props.max}
          start={this.props.start}
          end={this.props.end}
          onStartChange={this.handleStartChange}
          onEndChange={this.handleEndChange}
        />
        <SmButton className="sm-slider-form__submit-button" type="submit">Применить</SmButton>
      </form>
    );
  }
}