import React from 'react';
import './SmButton.css';

export default class SmButton extends React.Component {
  render() {
    return (
      <button onClick={this.props.onClick} className={`sm-button ${this.props.className}`} type={this.props.type || 'button'}>{this.props.children}</button>
    );
  }
}